<?php
# Home
Route::get('/', ['before'=>'auth','as'=>'home', 'uses'=>'SiteController@index']);
# Login
Route::get('login', ['as'=>'user.login', 'before'=>'already', 'uses'=>'UsersController@get_login']);
Route::post('login', ['as'=>'user.post.login', 'uses'=>'UsersController@post_login']);
Route::get('logout', ['as'=>'user.logout', 'uses'=>'UsersController@logout']);
# Passwords
Route::get('password/reset', ['uses' => 'PasswordController@remind',  'as' => 'password.remind']);
Route::post('password/reset', ['uses' => 'PasswordController@request', 'as' => 'password.request']);
Route::get('password/reset/{token}', ['uses' => 'PasswordController@reset', 'as' => 'password.reset']);
Route::post('password/reset/{token}', ['uses' => 'PasswordController@update', 'as' => 'password.update']);
# Cadastro
Route::get('cadastro', ['as'=>'user.register', 'uses'=>'UsersController@get_register']);
Route::post('cadastro', ['as'=>'user.post.register', 'uses'=>'UsersController@post_register']);
Route::get('coord', 'CustomersController@coord');
# Clientes
Route::any('clientes', 'CustomersController@getCustomers');
Route::post('clientes-concorrentes', 'CustomersController@companyClientsUpload');
# Administração
Route::group(['prefix'=>'admin','before'=>'auth|admin'], function(){
	
	Route::resource('clientes', 'CustomersController');
	
	Route::get('concorrentes', ['as'=>'admin.concorrentes.index', 'uses'=>'CustomersController@index']);
	
	Route::resource('empresas', 'CompaniesController');

	Route::resource('usuarios', 'UsersController');
	
	Route::resource('grupoprodutos', 'ProdgroupsController');

	# Upload clientes Knauf
	Route::post('upload/clientes/knauf',['as'=>'customers.upload', 'uses'=>'CustomersController@clientUpload']);
	Route::post('upload/concorrentes/knauf',['as'=>'concorrentes.upload', 'uses'=>'CustomersController@companyClientsUpload']);
	
	Route::get('aprovar', ['as'=>'admin.approve.user', 'uses'=>'UsersController@approve']);

	Route::get('mapa', 'CustomersController@mapa');

	Route::resource('vendas','TransactionsController');
	Route::post('upload/vendas',['as'=>'transaction.upload', 'uses'=>'TransactionsController@upload']);

	Route::get('set-coord/{num_ini?}/{num_fim?}', 'CustomersController@setCoords');
});

<?php 
/**
* Crud de grupo de produtos
*/
class ProdgroupsController extends BaseController
{
	public function index()
	{
		$prodgroups = Prodgroup::orderBy('id','desc')->paginate(20);
		return View::make('admin.prodgroups.index',compact('prodgroups'));
	}	
	public function create()
	{
		return View::make('admin.prodgroups.create');
	}
	public function store()
	{
		$data = Input::all();

		$prodgroup = Prodgroup::create($data);
		$prodgroup->accept = serialize($data['accept']);
		$prodgroup->save();

		return Redirect::route('admin.grupoprodutos.index')->withSuccess('Criado com sucesso');
	}
	public function edit($id)
	{
		$prodgroup = Prodgroup::find($id);
		return View::make('admin.prodgroups.edit', compact('prodgroup'));
	}
	public function update($id)
	{
		$data = Input::all();
		
		$prodgroup = Prodgroup::find($id);

		$prodgroup->update($data);
		$prodgroup->accept = serialize($data['accept']);
		$prodgroup->save();

		return Redirect::route('admin.grupoprodutos.edit', $prodgroup->id)->withSuccess('Editado com sucesso');
	}
	public function destroy($id)
	{
		$prodgroup = Prodgroup::find($id);
		
		$prodgroup->delete();

		return Redirect::route('admin.grupoprodutos.index')->withSuccess('Apagado com sucesso');
	}
}
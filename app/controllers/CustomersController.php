<?php

class CustomersController extends BaseController {

	//protected $layout = 'layouts.application';

	public function index()
	{
		$t = Route::current()->getName() == 'admin.clientes.index' ? '1' : '2';

		$c = Customer::whereType($t)->orderBy('id','desc');

		if(Input::has('company_id')){
			$c->where('company_id', Input::get('company_id'));
		}

		$customers = $c->paginate(25);

		$companies = Company::orderBy('name','ASC')->lists('name','id');


		return View::make('admin.customers.index', compact('customers','companies'));
	}

	public function getCustomers()
	{
		$data = Input::all();

		$loc = json_decode($data['currentBounds']); 
		$form = json_decode($data['form']); 

		$a = $loc->south;
		$b = $loc->west;
		$c = $loc->north;
		$d = $loc->east;

		/*$a = $data['south'];
		$b = $data['west'];
		$c = $data['north'];
		$d = $data['east'];*/

		// Query to find LatLng near by BoundFit from Google
		$cs = Customer::where(function($q) use($a,$c){
			$q->where(function($j) use($a, $c){
				$j->whereRaw('? < ?', array($a, $c))
			      ->whereBetween('lat', array($a, $c));
			})
			->orWhere(function($y) use($a, $c){
				$y->whereRaw('? < ?', array($c, $a))
				->whereBetween('lat',array($c, $a));
			});
		})
		->where(function($k) use($b,$d){
			$k->where(function($w) use($b, $d){
				$w->whereRaw('? < ?', array($b, $d))
			      ->whereBetween('lng', array($b, $d));
			})
			->orWhere(function($z) use($b, $d){
				$z->whereRaw('? < ?', array($d, $b))
				->whereBetween('lng',array($d, $b));
			});
		});
	
		if(isset($form[0])){
			$cs->where('CodCatCl', $form[0]->value);
		}

		// Verificando acesso do perfil vendedor e exibindo apenas empresas com transações referente a aeste eprfil
		$user = Sentry::getUser();
		if($user->group_id=='2'){
			$cs->whereHas('transactions', function($t_c) use($user){
				$t_c->where('sales_rep', $user->code);
			});
		}
		
		$customers = $cs->orderBy('id')->get();

		// Transações referentes aos clientes
		$t = Transaction::whereIn('customer_id', $customers->lists('CodCli'));

		// Filtrando transaçoes por grupos
		if(isset($form[1])){
			$t->where('group', 'like', '%'.$form[1]->value.'%');
		}
		if(isset($form[2])){
			switch ($form[2]->value) {
				case '1':
					$meses = 1;
				break;
				case '2':
					$meses = 3;
				break;
				case '3':
					$meses = 6;
				break;
				default:
					$meses = 1;
					break;
			}
			$hj = date('Y-m-d');
			$defDate = Helper::DiminuirMeses($hj, $meses);

			$t->where('transaction_at', '>=', $defDate)
				->where('transaction_at', '<=', $hj);
		}
		


		$transactions = $t->get();

		$total = $transactions->sum('receita');

		$m2 = $m = $kg = $cx = $rl = array();
		
		foreach ($transactions as $tr) {
			if($tr->um_basica_qtdvendas == 'M'){
				array_push($m, $tr->quantidade_de_vendas);
			}
			if($tr->um_basica_qtdvendas == 'M2'){
				array_push($m2, $tr->quantidade_de_vendas);
			}
			if($tr->um_basica_qtdvendas == 'KG'){
				array_push($kg, $tr->quantidade_de_vendas);
			}
			// Se for parafuso = caixas
			if($tr->prod_hierarquia == '33958300'){
				array_push($cx, 1);
			}
			// Se for rolo
			if($tr->prod_hierarquia == '33959500' || $tr->prod_hierarquia == '33959501' || $tr->prod_hierarquia == '33959502' || $tr->prod_hierarquia == '33959503' ){
				array_push($rl, 1);
			}	

		}

		$vendas_m = array_sum($m);
		$vendas_m2 = array_sum($m2);
		$vendas_kg = array_sum($kg);
		$vendas_cx = array_sum($cx);
		$vendas_rl = array_sum($rl);

		$numeros = array($total, $vendas_m, $vendas_m2, $vendas_kg, $vendas_cx, $vendas_rl);

		//print_r(DB::getQueryLog());

		return Response::json(array('customers'=>$customers, 'transactions'=> $transactions, 'numeros'=>$numeros));
		

	}
	public function clientUpload()
	{
		if(!File::isFile(Input::file('clientes'))){
			return Redirect::route('admin.clientes.index')->withError('Necessário subir um arquivo .txt válido');
		}
		
		$file = Input::file('clientes');
		$ext = $file->getClientOriginalExtension();
        $name = 'arquivoconsumo';
        $filename = $name.'.'.$ext;

        if(File::exists('uploads/clients', $filename)){
        	File::delete('uploads/clients', $filename);
        }
        
		$file->move('uploads/clients', $filename);

		Excel::load('uploads/clients/'.$filename, function($reader) {

		    $results = $reader->all();

		    //print_r($results);

			foreach ($results as $row) {
				
				$c = Customer::where('CodCli',$row->codcli)->first();
					if(isset($c)){
						$customer = $c;
					}else{
						$customer = new Customer;
						$customer->CodCli = $row->codcli;
					}
					
					$customer->Nome       = $row->nome; 
					$customer->Fantasia   = $row->fantasia; 
					$customer->Tel        = $row->tel; 
					$customer->Lograd     = $row->lograd; 
					$customer->Numero     = $row->numero; 
					$customer->Complem    = $row->complem; 
					$customer->Bairro     = $row->bairro; 
					$customer->Mun        = $row->mun; 
					$customer->Est        = $row->est; 
					$customer->Cep        = $row->cep; 
					$customer->Pais       = $row->pais; 
					$customer->Cnpj       = $row->cnpj; 
					$customer->Cpf        = $row->cpf; 
					$customer->InsEst     = $row->insest; 
					$customer->InsMun     = $row->insmun; 
					$customer->CodCatCl   = $row->codcatcl; 
					$customer->Codvend    = $row->codvend; 
					$customer->BlFinan    = $row->blFinan; 
					$customer->Tpfrete    = $row->tpfrete; 
					$customer->Regiao     = $row->regiao; 
					$customer->Condpag    = $row->condpag; 
					$customer->Email      = $row->e_mail; 
					$customer->LimCredito = $row->limcredito; 
					$customer->CompTotal  = $row->comptotal; 
					$customer->VlrAVenc   = $row->vlravenc; 
				    $customer->VlrVenci   = $row->vlrvenci; 
					$customer->VlrPedid   = $row->vlrpedid; 
					$customer->Seguro     = $row->seguro; 
					$customer->ListaPre   = $row->listapre; 

					$customer->type = "1";

					$customer->save();
					
			}

		});
		
		/*$arq = file_get_contents(public_path().'/uploads/clients/'.$filename);
		//action;CodCli;Nome;Fantasia;Tel;Lograd;Numero;Complem;Bairro;Mun;Est;Cep;Pais;Cnpj;Cpf;InsEst;InsMun;CodCatCl;Codvend;BlFinan;Tpfrete;Regiao;Condpag;E-mail;LimCredito;CompTotal;VlrAVenc;VlrVenci;VlrPedid;Seguro;ListaPre
		$base = explode(';', $arq);
		array_splice($base, 0, 31);
		
		$arr = array();


		for ($i=0; $i < sizeof($base); $i++) { 

			if ($i % 30 == 0){
		
				
				$n = substr($base[$i], 0, strpos($base[$i], 'C'));
				if($n != ''){
					array_push($arr, $n);
				}else{
					array_push($arr, $base[$i]);
				}
				
			}
		}

		foreach ($base as $k => $b) {
			foreach ($arr as $a) {
				if($b == $a){
					$c = Customer::where('CodCli',$base[$k])->first();
					if(isset($c)){
						$customer = $c;
					}else{
						$customer = new Customer;
					}
					
					$customer->CodCli = $base[$k];
					$customer->Nome = $base[$k+1];
					$customer->Fantasia = $base[$k+2];
					$customer->Tel = $base[$k+3];
					$customer->Lograd = $base[$k+4];
					$customer->Numero = $base[$k+5];
					$customer->Complem = $base[$k+6];
					$customer->Bairro = $base[$k+7];
					$customer->Mun = $base[$k+8];
					$customer->Est = $base[$k+9];
					$customer->Cep = $base[$k+10];
					$customer->Pais = $base[$k+11];
					$customer->Cnpj = $base[$k+12];
					$customer->Cpf = $base[$k+13];
					$customer->InsEst = $base[$k+14];
					$customer->InsMun = $base[$k+15];
					$customer->CodCatCl = $base[$k+16];
					$customer->Codvend = $base[$k+17];
					$customer->BlFinan = $base[$k+18];
					$customer->Tpfrete = $base[$k+19];
					$customer->Regiao = $base[$k+20];
					$customer->Condpag = $base[$k+21];
					$customer->Email = $base[$k+22];
					$customer->LimCredito = $base[$k+23];
					$customer->CompTotal = $base[$k+24];
					$customer->VlrAVenc = $base[$k+25];
					$customer->VlrVenci = $base[$k+26];
					$customer->VlrPedid = $base[$k+27];
					$customer->Seguro = $base[$k+28];
					$customer->ListaPre = $base[$k+29];
					
					// Pegando coordenadas
					$cords = $this->getCoords($base[$k+4].', '.$base[$k+5].', '.$base[$k+7].', '.$base[$k+8].' - '.$base[$k+9]);
					$customer->lat = $cords[0];
					$customer->lng = $cords[1];
					$customer->type = "1";

					$customer->save();
				}
			}
		}*/

		return Redirect::route('admin.clientes.index')->withSuccess('Clientes atualizados com sucesso');
	
	}

	/**
	* Serviço para pegar coordenadas
	* Google Maps
	* @param set-coord/{num_ini}/{num_fim}
	* @return json
	*/
	public function setCoords($num_ini = 0, $num_fim = 1000)
	{
		$customers = Customer::all();

		//print_r($customers);
		
		if($customers->count() > 0):
			$cont_1 = 0;
			$cont_2 = 0;

			foreach ($customers as $customer) {
				

				if($customer->type == '2'){
					$cont_1++;
					if( ($cont <= intval($num_fim)) and ($cont >= intval($num_ini)) ){
						// Pegando coordenadas
						// Se o cliente for concorrente ou Knauf formata diferente o endereço
						
							$cords = $this->getCoords($customer->Lograd.', '.$customer->Mun.' - '.$customer->Est);
						
					}
					
				}
				if($customer->type == '1'){
					$cont_2++;
					if( ($cont <= intval($num_fim)) and ($cont >= intval($num_ini)) ){
						$cords = $this->getCoords($customer->Lograd.', '.$customer->Numero.', '.$customer->Bairro.', '.$customer->Mun.' - '.$customer->Est);
					}
					
				}

						
				// Salvando as coordenadas
				$customer->lat = $cords[0];
				$customer->lng = $cords[1];
				$customer->save();
			}
			return Response::json(['response'=>'Clientes atualizados com sucesso']);
		endif;

		return Response::json(['response'=>'Não foram encontrados novos clientes para atualizar']);
	}

	public function companyClientsUpload()
	{
		if(!File::isFile(Input::file('clientes'))){
			return Redirect::route('admin.concorrentes.index')->withError('Necessário subir um arquivo .xls válido');
		}elseif(!Input::has('company_id')){
			return Redirect::route('admin.concorrentes.index')->withError('Selecione empresa');
		}
		
		$file = Input::file('clientes');
		$ext = $file->getClientOriginalExtension();
        $name = 'arquivoconsumo';
        $filename = $name.'.'.$ext;

        if(File::exists('uploads/concorrentes', $filename)){
        	File::delete('uploads/concorrentes', $filename);
        }
        
		$file->move('uploads/concorrentes', $filename);

		Excel::load('uploads/concorrentes/'.$filename, function($reader) {

		    $results = $reader->all();

			foreach ($results as $row) {
				$c = Customer::where('Nome',$row->nome)->first();
				if(isset($c)){
					$customer = $c;
				}else{
					$customer = new Customer;
				}

				$customer->Nome = trim($row->nome);
				$customer->Lograd = trim($row->endereco);
				$customer->Mun = trim($row->cidade);
				$customer->Est = trim($row->uf);
				$customer->Tel =  trim($row->telefone_1);
				$customer->Email =  trim($row->email_1);
				$customer->site =  trim($row->site);

				// Colocando ele como concorrente e setando a empresa
				$customer->type = "2";
				$customer->company_id = Input::get('company_id');

				$customer->save();
			}

		});
		return Redirect::route('admin.concorrentes.index')->withSuccess('Concorrentes atualizados com sucesso');
	}

	private function getCoords($address)
	{
		$geocode = Geocoder::geocode($address);

		if (!$cords = $geocode->getCoordinates()) {
			$cords = array(0,0);
		}
		
		return $cords;
		
	}

	public function coord()
	{
		$geocode = Geocoder::geocode('Av. Senador Raimundo Parente, 620 – Bairro da Paz, Manaus - AM');
		$cords = $geocode->getCoordinates();
		print_r($geocode);
	}
}

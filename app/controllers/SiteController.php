<?php

class SiteController extends BaseController {
	protected $layout = 'layouts.application';
	public function index()
	{
		$this->layout->content = View::make('customers.index');
	}
}

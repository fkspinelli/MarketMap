<?php

class TransactionsController extends BaseController {
	protected $layout = 'layouts.application';

	public function index()
	{
		$transactions = Transaction::orderBy('id','desc')->paginate(30);

		return View::make('admin.transactions.index', compact('transactions'));
	}

	public function create()
	{

	}

	public function edit($id)
	{
		$transaction = Transaction::find($id);
	}

	public function destroy($id)
	{
		$transaction = Transaction::find($id);
	}

	public function upload()
	{
		if(!File::isFile(Input::file('transactions'))){
			
			//print_r(Input::file('transactions'));
			return Redirect::route('admin.vendas.index')->withError('Necessário subir um arquivo .xslx válido');
		}

		$file = Input::file('transactions');
		$ext = $file->getClientOriginalExtension();
        $name = 'arquivoconsumo';
        $filename = $name.'.'.$ext;

        if(File::exists('uploads/vendas', $filename)){
        	File::delete('uploads/vendas', $filename);
        }
        
		$file->move('uploads/vendas', $filename);

		Excel::load('uploads/vendas/'.$filename, function($reader) {

		    $results = $reader->all();

		    //print($results);

			foreach ($results as $row) {
				$t = Transaction::where('no_documento',$row->no_documento)->first();
				if(isset($t->id)){
					// Edita
					$transaction = $t;
				}else{
					// Cria
					$transaction = new Transaction;
				}
				$transaction->no_documento = $row->no_documento;
				$transaction->customer_id = $row->cliente;
				$transaction->artigo = $row->artigo;
				$transaction->receita = $row->receita;
				$transaction->qtd_vendas = $row->qtd_vendas;
				$transaction->prod_hierarquia = $row->hierarq_produtos;
				$transaction->prod_group = $row->main_item_group;
				$transaction->prod_desc = $row->descricao_artigo;
				$transaction->customer_group = $row->grupo_de_clientes;
				$transaction->quantidade_de_vendas = $row->quantidade_de_vendas;
				$transaction->um_basica_qtdvendas = $row->um_basica_qtdvendas;
				$transaction->sales_rep = $row->sales_represent;
				$transaction->transaction_at = $row->data_da_fatura;
				$transaction->save();
				//echo $row->data_da_fatura.'<br/>';
			}

		});
		return Redirect::route('admin.vendas.index')->withSuccess('Vendas atualizadas com sucesso');
	}
}

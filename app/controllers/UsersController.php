<?php

class UsersController extends BaseController {

	protected $layout = 'layouts.application';

	public function index()
	{
		$users = User::orderBy('id','desc')->paginate(20);
		return View::make('admin.users.index', compact('users'));
	}
	public function create()
	{
		$groups = Group::orderBy('id','desc')->lists('name','id');
		return View::make('admin.users.create', compact('groups'));
	}
	public function store()
	{
		$data = Input::all();

		$validator = Validator::make($data, ['name'=>'required','email'=>'required|email|unique:users']);

		if($validator->fails()){
			return View::make('admin.usuarios.create')->withErrors($validator)->withInput();
		}

		$user = Sentry::create(Input::except(['group','password'])+['password'=>'mmp_knauf','activated'=>true]);

		$u = User::find($user->id);
		$u->group_id = $data['group'];
		$u->save();

		$resetCode = $user->getResetPasswordCode();

		Mail::send('emails.user.invite', ['user'=>$user, 'resetCode'=>$resetCode], function($m) use($user){
			$m->to($user->email, $user->name)->subject('Convite para participar da Knauf Market Map');

		});

		return Redirect::route('admin.usuarios.index')->withSuccess('Usuário criado com sucesso, enviamos um email para '.$user->email.' para que este posssa completar seu cadastro!');
	}
	public function edit($id)
	{
		$user = User::find($id);
		$groups = Group::orderBy('id','desc')->lists('name','id');

		return View::make('admin.users.edit', compact('user','groups'));
	}
	public function update($id)
	{
		$data = Input::all();

		$user = User::find($id);
		$user->update($data);

	
		return Redirect::route('admin.usuarios.edit', $user->id)->withSuccess('Usuário atualizado com sucesso');
	}

	public function destroy($id)
	{
		$user = User::find($id);

		$user->delete();

		return Redirect::route('admin.usuarios.index')->withSuccess('Usuário apagado com sucesso');

	}

	public function get_login()
	{
		return View::make('site.index');
	}
	public function post_login()
	{
		return $this->masterLogin('user.login','home');
	}
	public function logout()
	{
		return $this->masterLogout('user.login');
	}
	public function get_register()
	{
		return View::make('users.register');
	}
	public function post_register()
	{
		$data = Input::all();

		$validator = Validator::make($data, User::$rules);
		
		if($validator->fails()){
			return Redirect::route('user.register')->withErrors($validator)->withInput();
		}

		$u = Sentry::register([
			'email' => $data['email'],
			'password'=>$data['password'],
			'name' => $data['name'],
		]);

		$user = User::find($u->id);
		$user->approve_token = Hash::make(uniqid());
		$user->save();

		Mail::send('emails.user.register', ['user'=>$user], function($m){
			$m->subject('Novo usuário cadastrou-se no sistema Knauf Market Map, verificar pedido de inclusão.')
			->to('herculano@dizain.com.br', 'Knauf');
		});

		return Redirect::route('user.login')->withSuccess('Você solicitou cadastro para o sistema Knauf Market Map, aguarde aprovação de nossos administradores. Obrigado.');
	}

	public function approve()
	{
		$data = Input::all();
		$user = User::where('apporve_token',$data['token'])->first();

		if($user->activated != '0'){
			return Redirect::route('user.login')->withError('Usuário já esta ativado.');
		}

		$user->activated = $data['action'] == '1' ? '1' : '0';

		if($data['action'] == '1'){
			$user->activated_at = date('Y-m-d H:i:s');
			Mail::send('emails.user.approved', ['user'=>$user], function($m) use($user){
				$m->subject('Você foi aprovado no sistema Knauf Market Map')
				->to($user->email, $user->name);
			});
			return Redirect::route('user.login')->withSuccess('Usuario aprovado com sucesso');
		}else{
			Mail::send('emails.user.reproved', ['user'=>$user], function($m) use($user){
				$m->subject('Você não foi aprovado no sistema Knauf Market Map.')
				->to($user->email, $user->name);
			});
			return Redirect::route('user.login')->withSuccess('Usuario reprovado para o sistema');
		}

	}

}
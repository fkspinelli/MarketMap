<?php

class CompaniesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /companies
	 *
	 * @return Response
	 */
	public function index()
	{
		$companies = Company::orderBy('id','DESC')->paginate('20');

		return View::make('admin.companies.index', compact('companies'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /companies/create
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.companies.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /companies
	 *
	 * @return Response
	 */
	public function store()
	{
		$data = Input::all();

		$validator = Validator::make($data, Company::$rules);
		if($validator->fails()){
			return Redirect::route('admin.empresas.create')->withErrors($validator)->withInput();
		}

		$c = Company::create($data);

		return Redirect::route('admin.empresas.index')->withSuccess('Empresa criada com sucesso!');
	}

	/**
	 * Display the specified resource.
	 * GET /companies/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /companies/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

		
		$c = Company::find($id);

		return View::make('admin.companies.edit', compact('c'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /companies/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$data = Input::all();

		$validator = Validator::make($data, Company::$rules);
		if($validator->fails()){
			return Redirect::route('admin.empresas.update', $id)->withErrors($validator)->withInput();
		}
		$c = Company::find($id);

		$c->update($data);

		return Redirect::route('admin.empresas.edit',$c->id)->withSuccess('Empresa atualizada');
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /companies/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$c = Company::find($id);
		$c->delete();

		return Redirect::route('admin.empresas.index')->withSuccess('Empresa apagada');
	}

}
<?php
View::composer('partials.top', function($view) {
   $categories = Category::orderBy('id')->lists('name','id');
   $prod_groups = Prodgroup::orderby('id','desc')->lists('name','id');
   $view->with('categories',$categories)
   ->with('prod_groups',$prod_groups);
});
View::composer('partials.sidebar', function($view) {
  $categories = Category::orderBy('id')->get();
   $view->with('categories',$categories);
});

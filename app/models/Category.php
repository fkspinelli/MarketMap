<?php

class Category extends \Eloquent {
	protected $fillable = ['name'];

	public static $rules = ['name'=>'required'];

	public function customers()
	{
		return $this->hasMany('Customer','CodCatCl');
	}
	public function transactions()
	{
		return $this->hasMany('Transaction');
	}
}

<?php

class Transaction extends \Eloquent {
	protected $fillable = [];

	public function customer()
	{
		return $this->belongsTo('Customer','customer_id','CodCli');

	}
	public function vendedor()
	{
		return $this->belongsTo('User', 'sales_rep', 'code');
	}
}
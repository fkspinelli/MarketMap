<?php

class Company extends \Eloquent {
	protected $fillable = ['name'];

	public static $rules = ['name'=>'required'];

	public function customers()
	{
		return $this->hasMany('Customer');
	}
}
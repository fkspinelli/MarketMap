<?php

class Prodgroup extends \Eloquent {
	
	protected $table = 'product_groups';

	protected $fillable = ['name'];

	public static $rules = ['name'=>'required'];

	public function getAccept()
	{
		return unserialize($this->accept);
	}
}
<?php
class Customer extends \Eloquent {
	protected $fillable = ['action','CodCli','Nome','Fantasia','Tel','Lograd','Numero','Complem','Bairro','Mun','Est','Cep','Pais','Cnpj','Cpf','InsEst','InsMun','CodCatCl','Codvend','BlFinan','Tpfrete','Regiao','Condpag','E-mail','LimCredito','CompTotal','VlrAVenc','VlrVenci','VlrPedid','Seguro','ListaPre'];
	protected $appends = ['category_color'];
	public function company()
	{
		return $this->belongsTo('Company');
	}
	public function getCategoryColorAttribute()
	{
		$cat = Category::find($this->CodCatCl);
		return isset($cat->id) ? $cat->color : "04aee6";
		//return $this->CodCatCl;
	}
	public function category()
	{
		return $this->belongsTo('Category','CodCatCl');
	}

	public function transactions()
	{
		return $this->hasMany('Transaction','customer_id','CodCli');
	}

	public function user()
	{
		return $this->belongsTo('User', 'code', 'Codvend');
	}
}

<?php

class Group extends \Eloquent {
	protected $fillable = ['name'];

	public function users()
	{
		return $this->belongsToMany('User','users_groups');
	}
}
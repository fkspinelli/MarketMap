<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	protected $fillable = ['name','email','code','group_id'];

	public static $rules = ['name'=>'required','email'=>'required|email|unique:users','password'=>'required|confirmed'];

	public function groups()
	{
		return $this->belongsToMany('Group','users_groups');
	}

	public function vendas()
	{
		return $this->hasMany('Transaction', 'sales_rep','code');
	}

	public function customers()
	{
		$arr = array();
		foreach ($this->vendas as $v) {
			array_push($arr, $v->customer_id);
		}
		if(!empty($arr)):
		$clientes = Customer::whereIn('CodCli', $arr)->get();

		return $clientes;
	
		endif;

		return false;
	}
/*
pais tem muitos posts atraves de usuarios
'App\Post', 'App\User',
            'country_id', 'user_id', 'id'*/
}

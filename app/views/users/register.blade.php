@extends('layouts.login')
@section('content')
<div class="col-md-4 col-md-offset-4">
  <img src="{{ asset('images/logo-knauf-market-map.png') }}" alt="Market Map" />
  <h2>Cadastrar</h2>
  	
   @include('partials.notifications')

	{{ Form::open(['route'=>'user.post.register']) }}
	 <div class="form-group">
      <label for="email">Nome</label>
      {{ Form::text('name', null, ['class'=>'form-control']) }}
    </div>
    <div class="form-group">
      <label for="email">Email</label>
      {{ Form::email('email', null, ['class'=>'form-control']) }}
    </div>
    <div class="form-group">
      <label for="password">Senha</label>
       {{ Form::password('password', ['class'=>'form-control']) }}
    </div>
     <div class="form-group">
      <label for="password_confirmation">Confirmar senha</label>
       {{ Form::password('password_confirmation', ['class'=>'form-control']) }}
    </div>
    <div class="form-group">
      <a class="link pull-left" href="{{ route('user.login') }}">Voltar ao login</a>
  
      {{ Form::submit('Cadastrar', ['class'=>'btn btn-primary pull-right']) }}
    </div>
  {{ Form::close() }}
</div>
@stop
<aside class="aside-search">
    <div class="row logo">
        <div class="col-sm-12">

            <img src="/images/logo-knauf-market-map.png" class="img-responsive" alt="">
        </div>
    </div>
    <div class="row row-search">
        <div class="col-sm-12">
            <div class="inner-addon right-addon">
                <i class="icon icon-basic-magnifier"></i>
                <input type="text" class="form-control" placeholder="Faça sua busca">
            </div>
        </div>
    </div>
    <div class="row" style="margin-top:20px;">
        <div class="col-sm-12">
            <div class="panel-group filter" style="margin-bottom: -10px;">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" href="#collapse1"><i class="fa fa-filter" aria-hidden="true"></i> Filtrar</a>
                    </div>
                    <div id="collapse1" class="panel-collapse collapse">
                        <div class="panel-body">
                            <form role="form">
                                <div class="form-group">
                                    <select class="form-control form-filter">
                                        <option selected value="">Estado</option>
                                        <option value="x">RJ</option>
                                        <option value="x">RJ</option>
                                        <option value="x">MG</option>
                                        <option value="x">SP</option>
                                        <option value="x">RS</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select class="form-control form-filter">
                                        <option selected value="">Municipio</option>
                                        <option value="x">Duque de Caxias</option>
                                        <option value="x">Campo Grande</option>
                                        <option value="x">Realengo</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select class="form-control form-filter">
                                        <option selected value="">Categoria</option>
                                        <option value="x">Cliente</option>
                                        <option value="x">Concorrente 01</option>
                                        <option value="x">Concorrente 02</option>
                                        <option value="x">Concorrente 03</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select class="form-control form-filter">
                                        <option selected value="">Tipo</option>
                                        <option value="x">Construtora</option>
                                        <option value="x">Consumidor Final</option>
                                        <option value="x">Distribuidor</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select class="form-control form-filter">
                                        <option selected value="">Período</option>
                                        <option value="x">1 Mês</option>
                                        <option value="x">3 Meses</option>
                                        <option value="x">6 Meses</option>
                                        <option value="x">12 Meses</option>
                                    </select>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="periodo_inicio" placeholder="Início">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="periodo_fim" placeholder="Fim">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <select class="form-control form-filter">
                                        <option selected value="">Grupo Produto</option>
                                        <option value="x">Chapa</option>
                                        <option value="x">Perfil</option>
                                        <option value="x">Massa</option>
                                        <option value="x">Parafuso</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select class="form-control form-filter">
                                        <option selected value="">Tipo de Volume</option>
                                        <option value="x">Valor</option>
                                        <option value="x">Quantidade</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select class="form-control form-filter">
                                        <option selected value="">Unidade de Medida</option>
                                        <option value="x">M2</option>
                                        <option value="x">ML</option>
                                        <option value="x">PC</option>
                                        <option value="x">CX</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select class="form-control form-filter">
                                        <option selected value="">Tipo de Período</option>
                                        <option value="x">Atual</option>
                                        <option value="x">Comparativo</option>
                                    </select>
                                </div>
                                <a class="btn btn-primary btn-block">Filtrar</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <ul id="result" class="list-group">
                <!-- <li class="list-group-item">
                    <h3>Knauf</h3>
                    <h4>Condomínio das Garças</h4>
                    <p>Av. das Américas, 3216 - Barra da Tijuca - CEP 22789-005 - Rio de Janeiro - RJ - Brasil</p>
                    <p>
                        Responsável: Luah
                        <br>
                        55 21 98564-7668  |  luahleon@gmail.com
                    </p>
                </li> -->
            </ul>
        </div>
    </div>
</aside>

<aside class="show">
    <div class="content">
      <i id="toggle-aside" class="fa fa-chevron-circle-left" aria-hidden="true"></i>
        <h4>Clientes Knauf</h4>
        <ul>
          @foreach($categories as $c)
  
            <li id="customer_cat_{{ $c->id }}" class="customer_cat" data-id="{{ $c->id }}"><i class="fa fa-map-marker" style="color: #{{ $c->color }};"></i> <b>0</b> <span> {{ $c->name }}</span></li>
          @endforeach
        </ul>

        <h4>Concorrentes e outros</h4>
        <ul id="obras-concorrentes">
            <li class="gypsun"><i class="fa fa-map-marker" style="color: #cd0000;"></i> <b>0</b> <span> Gypsun</span></li>
            <li class="placo"><i class="fa fa-map-marker" style="color: #3000ff;"></i> <b>0</b> <span> Placo</span></li>
            <li class="trevo"><i class="fa fa-map-marker" style="color: #318d46;"></i> <b>0</b> <span> Trevo</span></li>
            <li class="outras"><i class="fa fa-map-marker" style="color: #979a8f;"></i> <b>0</b> <span> Outros</span></li>
        </ul>

        <h4>Total</h4>
        <h5><span class="total_concorrentes">0</span> <span>Clientes</span></h5>
    </div>
</aside>

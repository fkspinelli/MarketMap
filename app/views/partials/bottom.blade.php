<footer>
    <div class="container-fluid">
        <div class="row">

            <div class="col-sm-2 col-sm-push-10">
                <div class="box-listagem">
                    <ul id="results">

                    </ul>
                </div>
                <div class="listagem"><i class="fa fa-list-alt" aria-hidden="true"></i> Listagem das empresas</div>
            </div>
        </div>
    </div>
</footer>
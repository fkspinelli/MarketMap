 @if($message = Session::get('success'))
   <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success">
                 {{ $message }}
            </div>
        </div>
    </div>
@endif

@if($message = Session::get('error'))
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger">
                 {{ $message }} 
            </div>
        </div>
    </div>
@endif

@if($errors->count()>0)
    <div class="row">
        <div class="col-md-12">
           
                  <div class="alert alert-danger">
                    <ul>
                    @foreach($errors->all() as $k => $error)
                        <li>{{ $error }}</li>

                        
                    @endforeach
                    </ul>
                </div> 
            
        </div>
    </div>
@endif
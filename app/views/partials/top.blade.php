{{-- <nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
          <img src="{{ asset('images/logo-knauf-market-map.png') }}" class="logo-market-map" alt="Market Map" />
        </div>
        <ul class="nav navbar-nav navbar">
            <li class="dropdown">
                <a id="user" class="dropdown-toggle size-18 text-light" data-toggle="dropdown" href="#">
                    <span>Olá, <b class="text-semi-bold">{{ Sentry::getUser()->name }}</b></span>
                    <span class="caret"></span>
                    <i class="fa fa-user" style="font-size: 21px; border: 1px solid #333; width: 30px; height: 30px; text-align: center; line-height: 27px; border-radius: 50%; "></i>
                </a>
                <ul class="dropdown-menu">
                    <!--<li><a href="#">Page 1-1</a></li>
                    <li><a href="#">Page 1-2</a></li>-->
                    @if(Sentry::getUser()->hasAccess('admin'))
                        <li><a href="{{ route('admin.clientes.index') }}">Administração</a></li>
                    @endif
                    <li><a href="{{ route('user.logout') }}">Sair</a></li>
                </ul>
            </li>
            <li id="icon-search"><a href="#" style="padding-bottom: 11px;"><i class="icon icon-basic-magnifier size-35" style="height: 24px;overflow: hidden;display: inline-block;margin-top: -5px;font-size: 24px;position: relative;top: 10px;"></i></a></li>
            <li><a href="#" style="padding-top: 13px; padding-bottom: 6px;"><span class="badge badge-message"><span>3</span><span></span></span></a></li>
            <li><a href="#" id="menu" style="padding-bottom: 11px;"><i class="icon icon-menu size-35" style="height: 24px; overflow: hidden; display: inline-block; margin-top: -6px;"></i></a></li>
        </ul>
    </div>
</nav> --}}

<header>
    <nav>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-2">
                    <img src="{{ asset('images/logo-knauf-market-map.png') }}" class="logo" alt="Market Map" />
                </div>
                <div class="col-sm-2">
                    <div class="dropdown">
                        <a href="#" id="user" class="dropdown-toggle size-18 text-light" data-toggle="dropdown">
                            <span class="user-name">Olá, <b class="text-semi-bold">{{ Sentry::getUser()->name }}</b></span>
                            <span class="caret"></span>
                            <i class="fa fa-user" style="font-size: 21px; border: 1px solid #333; width: 30px; height: 30px; text-align: center; line-height: 27px; border-radius: 50%; "></i>
                        </a>
                        <ul class="dropdown-menu">
                            @if(Sentry::getUser()->hasAccess('admin'))
                                <li><a href="{{ route('admin.clientes.index') }}">Administração</a></li>
                            @endif
                            <li><a href="{{ route('user.logout') }}">Sair</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="search inner-addon right-addon">
                        <i class="fa fa-search"></i>
                        <input type="text" id="endereco" class="form-control" placeholder="Barra da Tijuca, Rio de Janeiro">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="row">
                        <div class="col-sm-8 col-sm-push-4">
                            <div class="dropdown">
                                <a class="btn-filter" data-toggle="dropdown">
                                    <i class="fa fa-sliders" aria-hidden="true"></i> 
                                    Filtrar
                                </a>
                                <div class="dropdown-menu">

                                    <form action="#" id="frm_filter">
                                        <div class="form-group">
                                          {{ Form::select('cat[]',[''=>'Todos os tipos']+$categories, Input::get('cat'),['class'=>'form-control selectpicker','multiple', 'style'=>'height:100px;', 'id'=>'multi_tipo', 'data-selected-text-format'=>'count > 2']) }}
                                        </div>
                                        <div class="form-group">
                                             {{ Form::select('p',[''=>'Grupo de Produto']+$prod_groups, Input::get('p'),['class'=>'form-control']) }}
                                        </div>
                                        <div class="form-group">
                                            {{ Form::select('per',[''=>'Período','1'=>'1 Mês', '2'=>'3 Meses', '3'=>'6 Meses'], Input::get('per'),['class'=>'form-control']) }}

                                        </div>
                                       <!-- <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group inner-addon right-addon">
                                                    <i class="fa fa-calendar"></i>

                                                    
                                                    {{ Form::text('from', Input::get('from'), ['class'=>'form-control', 'id'=>'from','placeholder'=>'De']) }}

                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group inner-addon right-addon">
                                                    <i class="fa fa-calendar"></i>

                                                    
                                                    {{ Form::text('to', Input::get('to'), ['class'=>'form-control', 'id'=>'to','placeholder'=>'De']) }}

                                                </div>
                                            </div>
                                        </div> -->
                                        <button type="button" class="btn btn_frm_filter btn-primary">Filtrar</button>

                                    {{ Form::close() }}

                                </div>
                            </div>
                        </div>
                    </div>
                    <style type="text/css">
                    #frm_filter .bootstrap-select button {
                        padding: 10px 30px 10px 12px;
                        border: none !important;
                    }

                    #frm_filter .bootstrap-select.btn-group .dropdown-menu.inner {
                        max-height: 145px !important;
                    }
                    </style>
                    <div class="row">
                        <!--<div class="col-sm-12">
                            <div class="label-filter">
                                <span>CONSUMIDOR FINAL <i class="fa fa-close"></i></span>
                                <span>PARAFUSO <i class="fa fa-close"></i></span>
                                <span>01/08/16 ATÉ 31/08/16 <i class="fa fa-close"></i></span>
                            </div>
                        </div>-->
                    </div>
                </div>
                <div class="col-sm-2">

                    <div class="box-info">
                        <div id="clientes-knauf" class="numeros">

                            <div class="num numero">0</div>
                            <div class="text">clientes knauf</div>
                        </div>
                        <div class="data">
                            <div class="header">
                                <h4>Receita no período</h4>
                                <p>R$ <span class="valor_total">0</span></p>
                            </div>
                            <div class="content">
                                <div id="info" class="collapse">
                                    <div class="content">
                                        <h4>Volume no período</h4>
                                        <p><span class="total_m2">0</span> <small>m2</small></p>
                                        <p><span class="total_m">0</span> <small>m</small></p>
                                        <p><span class="total_K">0</span> <small>kg</small></p>
                                        <p><span class="total_C">0</span> <small>cx</small></p>
                                        <p><span class="total_R">0</span> <small>rl</small></p>
                                        
                                    </div>
                                </div>
                                <div class="bt-control" data-toggle="collapse" data-target="#info">
                                    <i class="fa fa-chevron-circle-down" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</header>

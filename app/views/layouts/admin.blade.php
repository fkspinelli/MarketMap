<!DOCTYPE>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Knauf MarketMap - Administração</title>
	{{ HTML::style('stylesheets/bootstrap.min.css') }}
	{{ HTML::style('stylesheets/datepicker.css') }}
	{{ HTML::style('stylesheets/font-awesome.min.css') }}
	{{ HTML::style('stylesheets/admin.css') }}
	
	{{ HTML::script('javascripts/jquery.min.js') }}
	
</head>
<body>
	 <div id="wrapper">
		
		@include('admin.partials.navigation')
		
        <div id="page-wrapper">

            <div class="container-fluid">
				
				@yield('content')

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

     <!-- Bootstrap Core JavaScript -->
	
	{{ HTML::script('javascripts/bootstrap.js') }}
	{{ HTML::script('javascripts/jquery-ui.js') }}
	
	
	{{ HTML::script('javascripts/admin.js') }}
</body>
</html>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Knauf - MarketMap</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('stylesheets/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('stylesheets/beauty-font.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('stylesheets/font-awesome.min.css') }}">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="{{ asset('stylesheets/application.css') }}">
    <script src="{{ asset('javascripts/jquery.min.js') }}"></script>
    <script src="{{ asset('javascripts/bootstrap.js') }}"></script>
    
    <script src="http://maps.googleapis.com/maps/api/js"></script>
    <script src="{{ asset('javascripts/script.js') }}"></script>
    <script src="{{ asset('javascripts/map.js') }}"></script>
</head>

<body>

    <div class="container">
        @yield('content')
    </div>
</body>

</html>

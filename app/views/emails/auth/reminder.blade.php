<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Gerar nova senha</h2>

		<div>
			Para gerar sua nova senha no sistema, clique no link a seguir. <a target="_blank" href="{{ route('password.reset', $resetCode) }}">{{ route('password.reset', $resetCode) }}</a><br/>
			Este link se expira em {{ Config::get('auth.reminder.expire', 60) }} minutos.
		</div>
	</body>
</html>

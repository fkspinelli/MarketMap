<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
	<h2></h2>
		<table>
			<tr>
				<td><b>Olá {{ $user->name }}</b>, você foi convidado para participar da nova plataforma Knauf Market Map.</td>
			</tr>
			<tr>
				<td>Você só poderá entrar se estiver logado, e por isso lhe enviarmos o link abaixo para que possa gerar <b>uma nova senha</b> para seu cadastro se completar no sistema.</td>
				
			</tr>
			<tr>
				<td>Clique no link a seguir <a href="{{ route('password.reset', $resetCode) }}">{{ route('password.reset', $resetCode) }}</a></td>
				{{ Config::get('auth.reminder.expire', 10080) }}
			</tr>
			<!--<tr>
				<td>* Este link expira em 7 dias, se estiver expirado, favor entre em contato com </td>
			</tr>-->
			
		</table>
	</body>
</html>

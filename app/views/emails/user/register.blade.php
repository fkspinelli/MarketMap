<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
	<h2>Novo usuário cadastrado</h2>
		<table>
			<tr>
				<td>Nome</td>
				<td>{{ $user->name }}</td>
			</tr>
			<tr>
				<td>Email</td>
				<td>{{ $user->email }}</td>
			</tr>
			<tr>
				<td colspan="2">
					Aprovar: <a href="{{ route('admin.approve.user', ['token'=>$user->approve_token, 'action'=>'1']) }}">Sim</a> ou <a href="{{ route('admin.approve.user', ['token'=>$user->approve_token, 'action'=>'2']) }}">Não</a>?
				</td>
			</tr>
		</table>
	</body>
</html>

@section('content')

<div id="page-esqueci">
	<div class="wrap clearfix">				
	
		<div class="col-md-4 col-md-offset-4">
		  <img src="{{ asset('images/logo-knauf-market-map.png') }}" alt="Market Map" />
		 
			<div class="chamada clearfix">						
				<h2>Esqueceu a senha de acesso?</h2>
				<p>Preencha o seu e-mail cadastrado para solicitar nova senha:</p>
				@include('partials.notifications')
				{{ Form::open(['route'=>'password.request']) }}
					<div class="form-group">
						<input type="email" name="email" class="form-control" id="email" placeholder="E-mail">
					</div>
				<a class="link pull-left" href="javaScript:window.history.back(-1);">Voltar</a>
				<button type="submit" class="enviar btn btn-primary  pull-right">Enviar</button>
				{{ Form::close() }} <!-- /form -->
			</div>
		</div> <!-- /cadastre-se -->

		<div class="col-md-6 col-sm-6 col-xs-12">
		</div> <!-- /col -->

	</div>
</div> <!-- /page -->
@stop
@section('content')
<div id="page-esqueci">
    <div class="wrap clearfix">
       <div class="col-md-4 col-md-offset-4">
        <img src="{{ asset('images/logo-knauf-market-map.png') }}" alt="Market Map" />
           
            <div class="chamada clearfix">
                <h2>Gere aqui a sua nova senha</h2>
                <p>Confirme seu senha a seguir, para que possa ser criada uma nova.</p>
                @include('partials.notifications') 
                {{ Form::open(['route'=>array('password.update', $token)]) }} 
                <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="Digite sua nova senha">
                </div>
                <div class="form-group">
                    <input type="password" name="password_confirmation" class="form-control" placeholder="Confirme seu nova senha">
                </div>
                <a class="link pull-left" href="javaScript:window.history.back(-1);">Voltar</a>
                <button type="submit" class="enviar btn btn-primary pull-right">Enviar <i class="icone-seta-direita"></i></button>
                {{ Form::close() }}
                <!-- /form -->
            </div>
        </div>
        <!-- /cadastre-s -->  
    </div>
</div>
<!-- /page -->
@stop

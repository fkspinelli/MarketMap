@extends('layouts.login')
@section('content')
<div class="col-md-4 col-md-offset-4">
  <img src="{{ asset('images/logo-knauf-market-map.png') }}" alt="Market Map" />
  <h2>Entrar</h2>
  	
   @include('partials.notifications')

	{{ Form::open(['route'=>'user.post.login']) }}
    <div class="form-group">
      <label for="email">Email</label>
      {{ Form::email('email', null, ['class'=>'form-control']) }}
    </div>
    <div class="form-group">
      <label for="password">Senha</label>
       {{ Form::password('password', ['class'=>'form-control']) }}
    </div>
    <div class="form-group">
      <a class="link pull-left" href="{{ route('password.remind') }}">Esqueci senha</a>
  
      {{ Form::submit('Entrar', ['class'=>'btn btn-primary pull-right']) }}
    </div>
  {{ Form::close() }}
  <div class="row">
    <div class="col-md-12">
      <p>
          Você pode se <a class="link" title="Sujeito a aprovação" href="{{ route('user.register') }}">cadastrar aqui.</a>
          Sujeito a aprovação.
      </p>
    </div>
  </div>
</div>

@stop
@extends('layouts.admin')
@section('content')
	<h1>Grupo de produtos <small>({{ $prodgroups->getTotal() }})</small></h1>
	<div class="row">
		<div class="col-md-12">
			<a class="btn btn-primary" href="{{ route('admin.grupoprodutos.create') }}">novo <i class="fa fa-plus"></i></a>
		</div>
	</div>
	@include('partials.notifications')
	<div class="row">
		<div class="col-md-12">
			<table class="table table-striped table-bordered">
				<tr>
					<th>Nome</th>
					<th>Aceita Ids</th>
					<th>Editar</th>
					<th>Apagar</th>
				</tr>
				@foreach($prodgroups as $g)
				<tr>
					<td>{{ $g->name }}</td>
					<td>{{ $g->getAccept() }}</td>
					<td><a class="btn btn-info" href="{{ route('admin.grupoprodutos.edit', $g->id) }}"><i class="fa fa-edit"></i></a></td>
					<td>
						{{ Form::open(['route'=>array('admin.grupoprodutos.destroy', $g->id), 'method'=>'delete']) }}

						{{ Form::button('<i class="fa fa-trash"></i>', ['type'=>'submit']) }}
						{{ Form::close() }}
					</td>
				
				</tr>
				@endforeach
			</table>
			{{ $prodgroups->appends($_GET)->links() }}
		</div>
	</div>
@stop
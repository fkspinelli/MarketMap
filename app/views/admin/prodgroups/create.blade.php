@extends('layouts.admin')
@section('content')
	<h1>Novo grupo de produtos</h1>
	
	<div class="row">
		<div class="col-md-8">
		@include('partials.notifications')
		{{ Form::open(['route'=>'admin.grupoprodutos.store']) }}
		
			<div class="form-group">
		      <label for="email">Nome</label>
		      {{ Form::text('name', null, ['class'=>'form-control']) }}
		    </div>
		    <div class="form-group">
		      <label for="accept">Ids separados por virgula</label>
		      {{ Form::textarea('accept', null, ['class'=>'form-control']) }}
		    </div>
		     <div class="form-group text-right">
		      <label for=""></label>
		      {{ Form::submit('Enviar',  ['class'=>'btn btn-primary']) }}
		    </div>
		  
		{{ Form::close() }}
		<span class="pull-right">
			<a class="link" href=""><i class="fa fa-arrow-left"></i>Voltar</a>
		</span>
		</div>
		
	</div>
@stop
@extends('layouts.admin')
@section('content')
	<h1>Editar grupo de produtos</h1>
	
	<div class="row">
		<div class="col-md-8">
		@include('partials.notifications')
		{{ Form::open(['route'=>array('admin.grupoprodutos.update', $prodgroup->id), 'method'=>'put']) }}
		
			<div class="form-group">
		      <label for="email">Nome</label>
		      {{ Form::text('name', $prodgroup->name, ['class'=>'form-control']) }}
		    </div>
		    <div class="form-group">
		      <label for="accept">Ids separados por virgula</label>
		      {{ Form::textarea('accept', $prodgroup->getAccept(), ['class'=>'form-control']) }}
		    </div>
		     <div class="form-group text-right">
		      <label for=""></label>
		      {{ Form::submit('Atualizar',  ['class'=>'btn btn-primary']) }}
		    </div>
		  
		{{ Form::close() }}
		<span class="pull-right">
			<a class="link" href=""><i class="fa fa-arrow-left"></i>Voltar</a>
		</span>
		</div>
		
	</div>
@stop
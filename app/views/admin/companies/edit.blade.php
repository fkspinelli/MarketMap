@extends('layouts.admin')
@section('content')
	<h1>Empresa nova</h1>
	
	<div class="row">
		<div class="col-md-12">
		@include('partials.notifications')
		{{ Form::open(['route'=>array('admin.empresas.update',$c->id), 'method'=>'put']) }}
			<table class="table table-striped table-bordered">
				<tr>
					<td>Nome</td>
					<td>{{ Form::text('name', $c->name, ['class'=>'form-control']) }}</td>
				</tr>
				<tr>
					<td colspan="2" align="right">
						{{ Form::submit('Atualizar', ['class'=>'btn btn-primary']) }}
					</td>
				</tr>
			</table>
		{{ Form::close() }}
				<span class="pull-right">
			<a class="link" href="javaScript:window.history.back(-1);"><i class="fa fa-arrow-left"></i>Voltar</a>
		</span>
		</div>
		
	</div>
@stop
@extends('layouts.admin')
@section('content')
	<h1>Empresa nova</h1>
	
	<div class="row">
		<div class="col-md-12">
		@include('partials.notifications')
		{{ Form::open(['route'=>'admin.empresas.store']) }}
			<table class="table table-striped table-bordered">
				<tr>
					<td>Nome</td>
					<td>{{ Form::text('name', null, ['class'=>'form-control']) }}</td>
				</tr>
				<tr>
					<td colspan="2" align="right">
						{{ Form::submit('Enviar', ['class'=>'btn btn-primary']) }}
					</td>
				</tr>
			</table>
		{{ Form::close() }}
		<span class="pull-right">
			<a class="link" href="javaScript:window.history.back(-1);"><i class="fa fa-arrow-left"></i>Voltar</a>
		</span>
		</div>
		
	</div>
@stop
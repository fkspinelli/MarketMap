@extends('layouts.admin')
@section('content')
	<h1>Empresas <small>{{ $companies->getTotal() }}</small></h1>
	<div class="row">
		<div class="col-md-12 text-right">
			<a class="btn btn-primary" href="{{ route('admin.empresas.create') }}">Novo</a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
		@include('partials.notifications')
			<table class="table table-striped table-bordered">
				<tr>
					
					<th>Nome</th>
					<th>Editar</th>
					<th>Apagar</th>
				</tr>
				@foreach($companies as $c)
				<tr>
					<td>{{ $c->name }}</td>
					<td><a class="btn btn-info" href="{{ route('admin.empresas.edit', $c->id) }}"><i class="fa fa-edit"></i></a></td>
					<td>
						{{ Form::open(['route'=>array('admin.empresas.destroy', $c->id), 'method'=>'delete']) }}
						{{ Form::button('<i class="fa fa-trash"></i>', ['class'=>'btn btn-danger', 'type'=>'submit']) }}
						{{ Form::close() }}
					</td>
					
				</tr>
				@endforeach
			</table>
			{{ $companies->appends($_GET)->links() }}
		</div>
	</div>
@stop
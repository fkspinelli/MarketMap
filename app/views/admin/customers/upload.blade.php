<div class="row">
	<div class="col-md-12">
	@include('partials.notifications')
		<div class="row">
			<div class="col-md-8">
				{{ Form::open(['route'=> Route::current()->getName() == 'admin.clientes.index' ? 'customers.upload' : 'concorrentes.upload', 'class'=>'form-inline frm_up', 'files'=>true]) }}
				{{ Form::file('clientes',['class'=>'form-control']) }}
				@if(Route::current()->getName() == 'admin.concorrentes.index')
					{{ Form::select('company_id', [''=>'Selecione empresa']+$companies, null, ['class'=>'form-control']) }}
				@endif
				{{ Form::button('<i class="fa fa-upload"></i> Subir arquivo', ['class'=>'btn btn_up btn-primary', 'type'=>'submit']) }}
				{{ Form::close() }}
				@include('admin.partials.loader')
			</div>
			@if(Route::current()->getName() == 'admin.concorrentes.index')
				<div class="text-right col-md-4">
					{{ Form::open(['route'=>'admin.concorrentes.index', 'method'=>'get', 'class'=>'form-inline']) }}
					<span>Buscar por:</span>
					{{ Form::select('company_id', [''=>'Selecione empresa']+$companies, Input::get('company_id'), ['class'=>'form-control']) }}
					{{ Form::button('<i class="fa fa-search"></i> Buscar', ['class'=>'btn btn-primary', 'type'=>'submit']) }}
					{{ Form::close() }}
				</div>
			@endif
		</div>
		
	</div>
</div>
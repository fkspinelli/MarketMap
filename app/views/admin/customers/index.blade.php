@extends('layouts.admin')
@section('content')
	<h1>{{ Route::current()->getName() == 'admin.clientes.index' ? 'Clientes' : 'Concorrentes' }} <small>({{ $customers->getTotal() }})</small></h1>
	@include('admin.customers.upload')
	<div class="row">
		<div class="col-md-12">
			<table class="table table-striped table-bordered">
				<tr>
					<th>CodCli</th>
					<th>Nome</th>
					<th>Tel</th>
					<th>Logradouro</th>
					<th>Cep</th>
					<th>Cpf/Cnpj</th>
				</tr>
				@foreach($customers as $customer)
				<tr>
					<td>{{ $customer->CodCli }}
					
					</td>
					<td>{{ $customer->Nome }}<br/>{{ $customer->Fantasia }}</td>
					<td>{{ $customer->Tel }}</td>
					<td>{{ $customer->Lograd }},{{ $customer->Numero }} - {{ $customer->Complem }}<br/>
						{{ $customer->Mun }} - {{ $customer->Est }}
					</td>
					<td>{{ $customer->Cep }}</td>
					<td>{{ $customer->Cnpj }} - {{ $customer->cpf }}</td>
				</tr>
				@endforeach
			</table>
			{{ $customers->appends($_GET)->links() }}
		</div>
	</div>
@stop
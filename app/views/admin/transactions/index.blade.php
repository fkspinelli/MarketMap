@extends('layouts.admin')
@section('content')
	<h1>Vendas <small>({{ $transactions->getTotal() }})</small></h1>
	
	@include('admin.transactions.upload')
	<div class="row">
		<div class="col-md-12">
		
			<table class="table table-striped table-bordered">
				<tr>
					
					<th>Nome cliente</th>
					<th>Descrição</th>
					<th>Editar</th>
					<th>Apagar</th>
				</tr>
				@foreach($transactions as $t)
				<tr>
					<td>{{ isset($t->customer->id) ? $t->customer->Nome : null }}</td>
					<td>{{ $t->prod_desc }}</td>
					<td><a class="btn btn-info" href="{{ route('admin.vendas.edit', $t->id) }}"><i class="fa fa-edit"></i></a></td>
					<td>
						{{ Form::open(['route'=>array('admin.vendas.destroy', $t->id), 'method'=>'delete']) }}
						{{ Form::button('<i class="fa fa-trash"></i>', ['class'=>'btn btn-danger', 'type'=>'submit']) }}
						{{ Form::close() }}
					</td>
					
				</tr>
				@endforeach
			</table>
			{{ $transactions->appends($_GET)->links() }}
		</div>
	</div>
@stop
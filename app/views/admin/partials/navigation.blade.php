        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="">
   
                <img height="30" src="{{asset('images/logo-knauf-market-map.png')}}" alt=""></a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
         
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {{ Sentry::getUser()->name }} <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ route('home') }}"><i class="fa fa-fw fa-files-o"></i> Front Office</a>
                        </li>
                       
                        <li class="divider"></li>
                        <li>
                            <a href="{{route('user.logout')}}"><i class="fa fa-fw fa-power-off"></i> Sair</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
               
                <ul class="nav navbar-nav side-nav">

                    <li class="active">
                        <a href="{{ route('admin.clientes.index') }}"><i class="fa fa-fw fa-dashboard"></i> Clientes</a>
                    </li>
                     <li class="active">
                        <a href="{{ route('admin.concorrentes.index') }}"><i class="fa fa-fw fa-dashboard"></i> Concorrentes</a>
                    </li>
                    <li class="active">
                        <a href="{{ route('admin.empresas.index') }}"><i class="fa fa-fw fa-dashboard"></i> Empresas</a>
                    </li>
                    <li class="active">
                        <a href="{{ route('admin.vendas.index') }}"><i class="fa fa-fw fa-dashboard"></i> Vendas</a>
                    </li>
                       <li class="active">
                        <a href="{{ route('admin.grupoprodutos.index') }}"><i class="fa fa-fw fa-users"></i> Grupo prod.</a>
                    </li>
                      <li class="active">
                        <a href="{{ route('admin.usuarios.index') }}"><i class="fa fa-fw fa-users"></i> Usuários</a>
                    </li>
                   
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
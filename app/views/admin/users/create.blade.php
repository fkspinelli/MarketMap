@extends('layouts.admin')
@section('content')
	<h1>Novo usuário</h1>
	
	<div class="row">
		<div class="col-md-8">
		@include('partials.notifications')
		{{ Form::open(['route'=>'admin.usuarios.store']) }}
			<div class="form-group">
				<label for="grupo">Perfil</label>
				{{ Form::select('group', [''=>'Selecione tipo de perfil']+$groups, null, ['class'=>'form-control']) }}
			</div>
			<div class="form-group" style="display: none;" id="code_block">
		      <label for="code">Código do representante</label>
		      {{ Form::text('code', null, ['class'=>'form-control']) }}
		    </div>
			 <div class="form-group">
		      <label for="name">Nome</label>
		      {{ Form::text('name', null, ['class'=>'form-control']) }}
		    </div>
		    <div class="form-group">
		      <label for="email">Email</label>
		      {{ Form::email('email', null, ['class'=>'form-control']) }}
		      <small>* senha padrão caso o usuário não receba o email de boas vindas - "mmp_knauf"</small>
		    </div>
		     <div class="form-group text-right">
		      <label for=""></label>
		      {{ Form::submit('Enviar',  ['class'=>'btn btn-primary']) }}
		    </div>
		  
		{{ Form::close() }}
		<span class="pull-right">
			<a class="link" href=""><i class="fa fa-arrow-left"></i>Voltar</a>
		</span>
		</div>
		
	</div>
@stop
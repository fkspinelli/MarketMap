@extends('layouts.admin')
@section('content')
	<h1>Usuários <small>{{ $users->getTotal() }}</small></h1>
	<div class="row">
		<div class="col-md-12 text-right">
			<a class="btn btn-primary" href="{{ route('admin.usuarios.create') }}">Novo</a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
		@include('partials.notifications')
			<table class="table table-striped table-bordered">
				<tr>
					
					<th>Nome</th>
					<th>Email</th>
					<th>Perfil</th>
					<th>Editar</th>
					<th>Apagar</th>
				</tr>
				@foreach($users as $u)
				<tr>
					<td>{{ $u->name }}</td>
					<td>{{ $u->email }}</td>
					<td>
						@if($u->group_id == '2')
							<span>Vendas</span>
							<?php //print_r($u->vendas()); ?>
							
							
						@else
							<span>Admin</span>
						@endif
					</td>
					<td><a class="btn btn-info" href="{{ route('admin.usuarios.edit', $u->id) }}"><i class="fa fa-edit"></i></a></td>
					<td>
						{{ Form::open(['route'=>array('admin.usuarios.destroy', $u->id), 'method'=>'delete']) }}
						{{ Form::button('<i class="fa fa-trash"></i>', ['class'=>'btn btn-danger', 'type'=>'submit']) }}
						{{ Form::close() }}
					</td>
					
				</tr>
				@endforeach
			</table>
			{{ $users->appends($_GET)->links() }}
		</div>
	</div>
@stop
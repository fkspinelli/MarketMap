@extends('layouts.admin')
@section('content')
	<h1>Editar usuário</h1>
	
	<div class="row">
		<div class="col-md-8">
		@include('partials.notifications')
		{{ Form::open(['route'=>array('admin.usuarios.update', $user->id), 'method'=>'put']) }}
			<div class="form-group">
				<label for="grupo">Perfil</label>
				{{ Form::select('group_id', [''=>'Selecione tipo de perfil']+$groups, $user->group_id, ['class'=>'form-control']) }}
			</div>
			<div class="form-group" style="{{ $user->group_id != '2' ? 'display: none;' : null }}" id="code_block">
		      <label for="code">Código do representante</label>
		      {{ Form::text('code', $user->code, ['class'=>'form-control']) }}
		    </div>
			 <div class="form-group">
		      <label for="email">Nome</label>
		      {{ Form::text('name', $user->name, ['class'=>'form-control']) }}
		    </div>
		    <div class="form-group">
		      <label for="email">Email</label>
		      {{ Form::email('email', $user->email, ['class'=>'form-control']) }}
		    </div>
		     <div class="form-group text-right">
		      <label for=""></label>
		      {{ Form::submit('Enviar',  ['class'=>'btn btn-primary']) }}
		    </div>
		  
		{{ Form::close() }}
		<span class="pull-right">
			<a class="link" href="{{ route('admin.usuarios.index') }}"><i class="fa fa-arrow-left"></i>Voltar</a>
		</span>
		</div>
		
	</div>
@stop
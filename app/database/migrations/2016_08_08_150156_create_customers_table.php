<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('action');
			$table->integer('CodCli');
			$table->string('Nome');
			$table->string('Fantasia');
			$table->string('Tel');
			$table->string('Lograd');
			$table->integer('Numero');
			$table->string('Complem');
			$table->string('Bairro');
			$table->string('Mun');
			$table->string('Est');
			$table->string('Cep');
			$table->string('Pais');
			$table->string('Cnpj');
			$table->string('Cpf');
			$table->string('InsEst');
			$table->string('InsMun');
			$table->integer('CodCatCl');
			$table->integer('Codvend');
			$table->string('BlFinan');
			$table->string('Tpfrete');
			$table->string('Regiao');
			$table->string('Condpag');
			$table->string('E-mail');
			$table->integer('LimCredito');
			$table->integer('CompTotal');
			$table->integer('VlrAVenc');
			$table->integer('VlrVenci');
			$table->integer('VlrPedid');
			$table->integer('Seguro');
			$table->string('ListaPre');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customers');
	}

}

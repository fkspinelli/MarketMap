$(document).ready(function() {

    $(window).on('load resize', function() {

        $('section.map, aside').height($(window).height() - 100);

        $('.box-listagem')
        .height($(window).height() - $('.box-info .numeros').height() - $('footer').height() - 46)
        .width($('.box-info').width() - 20)
        .css({'top': $('.box-info .numeros').height() + 42 });
    });

    $('.spinner').show();

    $( "#periodo_inicio, #periodo_fim" ).datepicker();

    $('.listagem').click(function(){
    	$('.box-listagem').toggle('slide', {direction: 'down'}, 'fast');
    });

    $('#toggle-aside').click(function(){
        if($('aside').hasClass('show')){
            $('aside').removeClass('show')
        }else{
            $('aside').addClass('show')
        }
    });


    $('.selectpicker').selectpicker();
    $('#frm_filter .bootstrap-select .dropdown-menu').hide();
    $('#frm_filter .bootstrap-select button').click(function(){
         $('#frm_filter .bootstrap-select .dropdown-menu').toggle();
        console.log($('.selectpicker').val());
    });



    $('.dropdown .btn-filter + .dropdown-menu').on('click', function(event){
        // The event won't be propagated up to the document NODE and 
        // therefore delegated events won't be fired
        event.stopPropagation();
    });




     //$('#multi_tipo').multipleSelect();

});

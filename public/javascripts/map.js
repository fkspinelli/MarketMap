$(document).ready(function(){
    $('.btn_frm_filter').click(function(){
        var form = $('#frm_filter').serializeArray();
        var cat = $('select[name="cat[]"]').val();
        
        // fechando o bloco de busca
        $('.dropdown').removeClass('open');
        $('.btn-filter').prop('aria-expanded',false);
        
        if(cat != ''){
            $('.customer_cat').fadeOut();
        
        
            $.each(cat, function(i,v){
                  $('#customer_cat_'+v).fadeIn();
            });
        }else{
            $('.customer_cat').fadeIn();

        }
        getUltData(form);
        filterMarkers(cat);
    });
});

var geocoder;
var map;
var infoWindow;
var autocomplete;
var markers = [];
var lookup = [];

Number.prototype.format = function(n, x, s, c) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
        num = this.toFixed(Math.max(0, ~~n));

    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};

function initMap() {
    
    var center = { lat: -22.8779247, lng:-43.2733609 };
   
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15,
        center: center,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true,
        zoomControl: true,
        zoomControlOptions: {
            position: google.maps.ControlPosition.RIGHT_CENTER
        },
    });

    window.resize;

    // uma nova janela de informações é criado
    infoWindow = new google.maps.InfoWindow();

    // Evento que fecha a janela de informações com um clique no mapa
    google.maps.event.addListener(map, 'click', function(event) {
        infoWindow.close();
    });

    $(document).on('click', '#closeInfo', function(){
        infoWindow.close();
    });

    // Faz algo quando a janela de exibição do mapa é alterada
    map.addListener('idle', function() {
        getData();
    });

    map.addListener('bounds_changed', function() {
        customInfoWindow();
    });

    map.addListener('tilesloaded', function() {
        $('.spinner').remove();
    });


    var input = (document.getElementById('endereco'));

    autocomplete = new google.maps.places.Autocomplete(input);

    google.maps.event.addListener(autocomplete, 'place_changed', function() {
        var place = autocomplete.getPlace();
        console.log(place);
         map.setCenter(place.geometry.location);
        /*for (var i = 0; i < place.address_components.length; i++) {
            for (var j = 0; j < place.address_components[i].types.length; j++) {
                if (place.address_components[i].types[j] == "postal_code") {
                    map.setCenter(place.geometry.location);
                    console.log(place.geometry.location);
                }
            }
        }*/
    });

}
google.maps.event.addDomListener(window, 'load', initMap);



function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}
function showPosition(position) {
  console.log(position.coords.latitude);
}

function getData() {
    var form = null;
    // alert(map.getBounds());
    // esta variável define os limites do mapa de acordo com a posição marcadores
    var bounds = new google.maps.LatLngBounds();

    // pega área visivel do mapa
    var currentBounds = map.getBounds();
    var sum = 0;
    var sum_mt = 0;
    var sum_mt2 = 0;
    var sum_kg = 0;
   
    // Faz requisição ao servidor pedindo os dados
    $.ajax({
        url: "/clientes",
        type: 'post',
        dataType: 'json',
        data: {'currentBounds': JSON.stringify(currentBounds), 'form': JSON.stringify(form)},
        success: function(result) {
            //console.log(result);
            $('.numero').text(0);

            // Loop para clientes
            for (i = 0; i < result.customers.length; i++) {

                var latlng = new google.maps.LatLng(result.customers[i].lat, result.customers[i].lng);
                var titulo1 = result.customers[i].Fantasia;
                var titulo2 = result.customers[i].Nome;
                var endereco = result.customers[i].Lograd + ", "+ result.customers[i].Numero + ", "+ result.customers[i].Complem + ", " +result.customers[i].Bairro+ ", " +result.customers[i].Mun+ " - " +result.customers[i].Est;
                var telefone = result.customers[i].Tel;
                var email = result.customers[i].Email;

                var cl = "";
                if(result.customers[i].type == "1"){

                    cl = "#04aee6"; //clientes knauf
                }else if(result.customers[i].type == "2"){
                    //concorrentes
                    if(result.customers[i].company_id == "1"){
                        cl = "#cd0000"; //Gypsun
                    }else if(result.customers[i].company_id == "2"){
                        cl = "#3000ff"; //Placo
                    }else if(result.customers[i].company_id == "3"){
                        cl = "#318d46"; //Trevo
                    }
                }else{
                    cl = "#979a8f"; //outros
                }
                var markerColor = cl;
                var tipo = result.customers[i].type;
                var company_id = result.customers[i].company_id;
                var categoryColor = result.customers[i].category_color;
                var CodCatCl = result.customers[i].CodCatCl;
                var id = result.customers[i].id;

                // insere novo marcador se esta na area visive do mapa e ainda não foi inserido
                if (currentBounds.contains(latlng) && isLocationFree([result.customers[i].lat, result.customers[i].lng])) {
                    lookup.push([result.customers[i].lat, result.customers[i].lng]);
                    addMarker(latlng, titulo1, titulo2, endereco, telefone, email, tipo, company_id, markerColor,categoryColor,CodCatCl,id);
                }


                // posição do marcador é adicionado à variável bounds
                bounds.extend(latlng);
            }

           
            $('.valor_total').text(result.numeros[0].format(2, 3, '.', ','));
            $('.total_m2').text(result.numeros[2].format(2, 3, '.', ','));
            $('.total_m').text(result.numeros[1].format(2, 3, '.', ','));
            $('.total_k').text(result.numeros[3].format(2, 3, '.', ','));
            $('.total_C').text(result.numeros[4]);
            $('.total_R').text(result.numeros[5]);

            showInfo();

        }
    });
}

// Adds a marker to the map and push to the array.
function addMarker(latlng, titulo1, titulo2, endereco, telefone, email, tipo, company_id, markerColor,categoryColor, CodCatCl, id) {
  var destColor = "#"+categoryColor;
  if(markerColor != "#04aee6"){
    destColor = markerColor;
  }

  
    // pin customizado
    var icon = {
        path: "M322.621,42.825C294.073,14.272,259.619,0,219.268,0c-40.353,0-74.803,14.275-103.353,42.825 c-28.549,28.549-42.825,63-42.825,103.353c0,20.749,3.14,37.782,9.419,51.106l104.21,220.986 c2.856,6.276,7.283,11.225,13.278,14.838c5.996,3.617,12.419,5.428,19.273,5.428c6.852,0,13.278-1.811,19.273-5.428 c5.996-3.613,10.513-8.562,13.559-14.838l103.918-220.986c6.282-13.324,9.424-30.358,9.424-51.106 C365.449,105.825,351.176,71.378,322.621,42.825z M270.942,197.855c-14.273,14.272-31.497,21.411-51.674,21.411 s-37.401-7.139-51.678-21.411c-14.275-14.277-21.414-31.501-21.414-51.678c0-20.175,7.139-37.402,21.414-51.675 c14.277-14.275,31.504-21.414,51.678-21.414c20.177,0,37.401,7.139,51.674,21.414c14.274,14.272,21.413,31.5,21.413,51.675 C292.355,166.352,285.217,183.575,270.942,197.855z",
        fillColor: destColor,
        fillOpacity: 1,
        anchor: new google.maps.Point(0, 0),
        strokeColor: brightness(destColor, -50),
        strokeWeight: 1,
        scale: 0.1
    }

    var marker = new google.maps.Marker({
        id: id,
        position: latlng,
        map: map,
        titulo1: titulo1,
        titulo2: titulo2,
        endereco: endereco,
        telefone: telefone,
        email: email,
        icon: icon,
        type: tipo,
        company_id: company_id,
        CodCatCl: CodCatCl
    });
    markers.push(marker);

    // Este evento espera um clique em um marcador
    // Quando este evento é disparado o conteúdo da janela de informações é criado
    // e a janela de informações é aberta.
    google.maps.event.addListener(marker, 'click', function(e) {

        // Criando o conteúdo a ser inserido na janela de informações
        var iwContent = '<div id="iw-container"><div class="iw-content"><div id="closeInfo" style="cursor: pointer;width: 20px;height: 20px;font-size: 20px;font-weight: bold;display: inline-block;position: absolute;right: 17px;top: 10px;">×</div><h4>' + titulo2 + '</h4><p>' + endereco + '</p><p>' + telefone + ' | ' + email + '</p></div></div>';

        // incluindo o conteúdo para  Info Window.
        infoWindow.setContent(iwContent);

        // abrir Info Window no mapa atual e no local do marcador atual.
        infoWindow.open(map, marker);



        var iwLeft = $('.gm-style-iw').offset().left;
        var iwTop = $('.gm-style-iw').offset().top;
        var boxHeight = $('.map .box-container').height();
        var asideWidth = $('aside').width();

        if (iwTop < boxHeight) {
            map.panBy(0, iwTop - boxHeight - 55);
        }
        if (iwLeft < (asideWidth + 50)) {
            map.panBy(iwLeft - (asideWidth + 50), 0);
        }

    });

    google.maps.event.addListener(infoWindow, 'domready', function(e) {
        customInfoWindow();
    });
}


function showInfo() {
    var type1 = 0;
    var type2 = 0;
    var type3 = 0;
    var gypsun = 0;
    var placo = 0;
    var trevo = 0;
    $('#results').html('');

  // Pegando o array de categorias
    var data_id;
    var arr_data_id = Array();
    $.each($('.customer_cat'), function(i,v){
        data_id = $(this).data('id');
       $('#customer_cat_'+data_id+' > b').text(0);
    });

    for (var i = 0; i < markers.length; i++) {

        var currentBounds = map.getBounds()
        var latlng = new google.maps.LatLng(markers[i].position.lat(), markers[i].position.lng());
        if(currentBounds.contains(latlng)){

            $('#results').append('<li><h4>' + markers[i].titulo1 + '</h4><h5>' + markers[i].titulo2 + '</h5><p>' + markers[i].endereco + '</p><p>Responsável:<b>' + markers[i].telefone + '  |  ' + markers[i].email + '</p></li>');

            var n = parseInt(markers[i].type);
            switch (n) {
                case 1:
                    type1++;
                    break;
                case 2:
                    type2++;

                    //concorrentes
                    if(markers[i].company_id == "1"){
                        gypsun++;
                    }else if(markers[i].company_id == "2"){
                        placo++;
                    }else if(markers[i].company_id == "3"){
                        trevo++;
                    }

                    break;
                case 3:
                    type3++;
                    break;
            }

            // Comparando e increentando numeros em categorias
            var s =  $('#customer_cat_'+markers[i].CodCatCl+' > b').text();
            var numb = isNaN(parseInt(s)) ? 0 : parseInt(s)
            numb++;
           
            $('#customer_cat_'+markers[i].CodCatCl+' > b').text(numb);
        }
    }
   
  //  $('#customer_cat_'+markers[i].CodCatCl+' b').text(num);
    $('#clientes-knauf .numero').text(type1);
    // $('#obras-concorrentes .numero').text(type2);
    $('#obras-concorrentes .gypsun b').text(gypsun);
    $('#obras-concorrentes .placo b').text(placo);
    $('#obras-concorrentes .trevo b').text(trevo);
    $('#obras-concorrentes .outras b').text(type3);
    $('.total_concorrentes').text(type1+type2+type3);

}
function filterMarkers(cat)
{

    for (var i = 0; i < markers.length; i++) {
        // replace apple with particular category
    
        if(cat == ''){
             markers[i].setVisible(true);
              
        }else{
            if(!!(cat.indexOf(String(markers[i].CodCatCl))+1)){
                markers[i].setVisible(true);
                 
            }
            else{
                markers[i].setVisible(false);
                
              
            }
        }
       
   }

   
}


function getUltData(form)
{
    var bounds = new google.maps.LatLngBounds();

    // pega área visivel do mapa
    var currentBounds = map.getBounds();

    console.log(JSON.stringify(currentBounds));
    console.log(form);

    // Faz requisição ao servidor pedindo os dados
    $.ajax({
        url: "/clientes",
        type: 'post',
        dataType: 'json',
        data: {'currentBounds': JSON.stringify(currentBounds), 'form': JSON.stringify(form)},
        success: function(res) {
            //console.log(res);
            $('.valor_total').text(res.numeros[0].format(2, 3, '.', ','));
            $('.total_m2').text(res.numeros[2].format(2, 3, '.', ','));
            $('.total_m').text(res.numeros[1].format(2, 3, '.', ','));
            $('.total_k').text(res.numeros[3].format(2, 3, '.', ','));
            $('.total_C').text(result.numeros[4]);
            $('.total_R').text(result.numeros[5]);
        }
    });
}
function brightness(hex, n) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);

    var r = parseInt(result[1], 16);
    var g = parseInt(result[2], 16);
    var b = parseInt(result[3], 16);

    var newR = r + (n);
    var newG = g + (n);
    var newB = b + (n);

    var color = 'rgb(' + newR + ', ' + newG + ', ' + newB + ')';

    return color;
}

function customInfoWindow() {
    $('.gm-style-iw').prev().children(':nth-child(1), :nth-child(2),:nth-child(4)').hide();
    $('.gm-style-iw').prev()
        .children(':nth-child(3)')
        .css({ 'transform': 'skewX(30.6deg)', 'transform-origin': '0px 0px 0px', 'z-index': '1', 'top': '105px', 'left': '173px' })
        .children('div')
        .css({ 'height': '90px', 'width': '18px' });

    $('.gm-style-iw').prev()
        .children(':nth-child(3)')
        .children(':nth-child(1)')
        .children('div')
        .css({ 'height': '90px', 'width': '18px', 'transform': 'skewX(5.6deg)', 'transform-origin': '0px 0px 0px', 'box-shadow': 'none', 'border-left': '3px solid #04aee6' });

    $('.gm-style-iw').prev()
        .children(':nth-child(3)')
        .children(':nth-child(2)')
        .children('div')
        .css({ 'height': '90px', 'width': '18px', 'transform': 'skewX(-9.8deg)', 'transform-origin': '0px 0px 0px', 'box-shadow': 'none', 'border-right': '3px solid #04aee6' });
}

function isLocationFree(search) {
    for (var i = 0, l = lookup.length; i < l; i++) {
        if (lookup[i][0] === search[0] && lookup[i][1] === search[1]) {
            return false;
        }
    }
    return true;
}

// Removes the overlays from the map, but keeps them in the array
function clearOverlays() {
  if (markers) {
    for (i in markers) {
      markers[i].setMap(null);
    }
  }
}



function DeleteMarkers() {
        //Loop through all the markers and remove
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
        markers = [];
    }
